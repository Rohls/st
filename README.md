# st - simple terminal
--------------------
My personal build of st, the simple terminal emulator for X which sucks less.

### Patches
* Clipboard
* Alpha

### Color schemes
* Gruvbox
* Amora


Requirements
------------
* Xlib headers
* ttf-Iosevka-terminal


Installation
------------ 
Configure `config.h` to your liking and then run:


    (sudo) make clean install


Running st
----------
If you did not install st with make clean install, you must compile
the st terminfo entry with the following command:

    tic -sx st.info

See the man page for additional details.

Credits
-------
Based on Aurélien APTEL <aurelien dot aptel at gmail dot com> bt source code.

